# TapiX Application

The Keboola application for TapiX by Dateio serves as a connector to the TapiX service developed by Dateio for processing and enriching bank transaction data.

**Table of contents:**  
  
[TOC]

## Configuration

The configuration of the application is pretty straightforward. The component requires 5 parameters and depending on the mode, an input table may be required.

A sample of the configuration can be found in the [component's repository](https://bitbucket.org/kds_consulting_team/kds-team.app-dateio-tapix/src/master/component_config/sample-config).

### Parameters

Following parameters are required by the application:

- `username` - a username to the TapiX API; beware, that this username may be different to the username used to access the API docs;
- `#password` - a password to the TapiX API; again, may be different to the password used to access the API docs;
- `mode` - specifies, in which mode the application will be launched;
- `incremental` - whether the outputs will be loaded into storage incrementally or not;
- `invalidations_since` - a date string in relative (e.g. 3 days ago), absolute (2020-01-01) format, or use keyword **last** to load latest data based on last saved state; the parameter is only needed for the `invalidations` mode.

#### Mode

The application can work in 4 different modes:

- `transactions` - utilizes endpoint `POST /shops/findByCardTransactionBatch` to find shops by transactions;
- `shops` - utilizes endpoint `GET /shops/{id}` to download details about shops;
- `merchants` - utilizes endpoint `GET /merchants/{id}` to download details about merchants;
- `invalidations` - utilizes endpoint `GET /invalidations` to download data about invalidations.

### Input Tables

All modes, except for `invalidations` mode, require a table on the input mapping to work properly.

For `transactions`, the table should be mapped to `transactions.csv` and should contain only column (attributes), which are supported by `/shops/findByCardTransactionBatch` endpoint. Additionally, `customId` column is required as the results need to be mapped back to their original records.

For `shops`, the table should be mapped to `shops.csv` and can contain arbitrary number of columns. Required column is `id`, which must contain the `uid` of the shop, for which the information should be downloaded.

For `merchants`, the table should be mapped to `merchants.csv` and can contain arbitrary number of columns. Required column is `id`, which must contain the `uid` of the merchant, for which the information should be downloaded.

## Development

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in the docker-compose file:

```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone repo_path my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

## Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/).