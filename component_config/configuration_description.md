A sample of the configuration can be found in the [component's repository](https://bitbucket.org/kds_consulting_team/kds-team.app-dateio-tapix/src/master/component_config/sample-config).

### Parameters

Following parameters are required by the application:

- `username` - a username to the TapiX API; beware, that this username may be different to the username used to access the API docs;
- `#password` - a password to the TapiX API; again, may be different to the password used to access the API docs;
- `mode` - specifies, in which mode the application will be launched;
- `incremental` - whether the outputs will be loaded into storage incrementally or not;
- `invalidations_since` - a date string in relative (e.g. 3 days ago), absolute (2020-01-01) format, or use keyword **last** to load latest data based on last saved state; the parameter is only needed for the `invalidations` mode.

#### Mode

The application can work in 4 different modes:

- `transactions` - utilizes endpoint `POST /shops/findByCardTransactionBatch` to find shops by transactions;
- `shops` - utilizes endpoint `GET /shops/{id}` to download details about shops;
- `merchants` - utilizes endpoint `GET /merchants/{id}` to download details about merchants;
- `invalidations` - utilizes endpoint `GET /invalidations` to download data about invalidations.

### Input Tables

All modes, except for `invalidations` mode, require a table on the input mapping to work properly.

For `transactions`, the table should be mapped to `transactions.csv` and should contain only column (attributes), which are supported by `/shops/findByCardTransactionBatch` endpoint. Additionally, `customId` column is required as the results need to be mapped back to their original records.

For `shops`, the table should be mapped to `shops.csv` and can contain arbitrary number of columns. Required column is `id`, which must contain the `uid` of the shop, for which the information should be downloaded.

For `merchants`, the table should be mapped to `merchants.csv` and can contain arbitrary number of columns. Required column is `id`, which must contain the `uid` of the merchant, for which the information should be downloaded.