# TapiX Application

The Keboola application for TapiX by Dateio serves as a connector to the TapiX service developed by Dateio for processing and enriching bank transaction data.

# Configuration

The configuration of the application is pretty straightforward. The component requires 5 parameters and depending on the mode, an input table may be required.

A sample of the configuration can be found in the [component's repository](https://bitbucket.org/kds_consulting_team/kds-team.app-dateio-tapix/src/master/component_config/sample-config).