import _csv
import csv
import logging
import os
import sys
import time
from datetime import datetime, timedelta
from glob import glob

import dateparser
from kbc.env_handler import KBCEnvHandler

from client import Client, DT_RFC_3399
from result import ResultWriter

CONFIG_USERNAME_KEY = 'username'
CONFIG_PASSWORD_KEY = '#password'
CONFIG_MODE_KEY = 'mode'
CONFIG_DEBUG_KEY = 'debug'
CONFIG_INVALIDATIONS_DATE = 'invalidations_date'
CONFIG_OUTPUT_KEY = 'output'

CONFIG_INVALIDATIONS_SINCE = 'since'
CONFIG_INVALIDATIONS_UNTIL = 'until'

CONFIG_OUTPUT_BUCKET = 'bucket'
CONFIG_OUTPUT_TABLE = 'table'
CONFIG_OUTPUT_INCREMENTAL = 'incremental'

MANDATORY_PARAMS = [CONFIG_USERNAME_KEY, CONFIG_PASSWORD_KEY, CONFIG_MODE_KEY]

ENV_RUNID_KEY = 'KBC_RUNID'
ENV_CMPID_KEY = 'KBC_COMPONENTID'
ENV_CFGID_KEY = 'KBC_CONFIGID'

APP_VERSION = '1.2.2'
sys.tracebacklimit = 0

ALLOWED_MODES = ['transactions', 'invalidations', 'merchants', 'shops']

TRANSACTIONS_ATTRIBUTES_CAST = {
    'ecommerceFlag': int,
    'ignoreTime': bool,
    'gpsLat': float,
    'gpsLong': float,
    'refresh': bool
}


class UserException(Exception):
    pass


class Component(KBCEnvHandler):
    TRANSACTIONS_BATCH_SIZE = 1000
    INVALIDATIONS_DEFAULT_RANGE_SINCE = '7 days ago'
    INVALIDATIONS_DEFAULT_RANGE_UNTIL = 'now'

    def __init__(self):
        super().__init__(MANDATORY_PARAMS, data_path=None)

        logging.info(f"Running app version {APP_VERSION}.")

        if self.cfg_params.get(CONFIG_DEBUG_KEY, False) is True:
            logger = logging.getLogger()
            logger.setLevel(level='DEBUG')

            sys.tracebacklimit = 3

        try:
            self.validate_config(mandatory_params=self._mandatory_params)
        except ValueError as e:
            logging.exception(e)
            sys.exit(1)

        self.client = Client(self.cfg_params[CONFIG_USERNAME_KEY], self.cfg_params[CONFIG_PASSWORD_KEY])
        self.mode = self.cfg_params[CONFIG_MODE_KEY]

        self.run_id = os.environ[ENV_RUNID_KEY]
        self.cmp_id = os.environ[ENV_CMPID_KEY]
        self.cfg_id = os.environ[ENV_CFGID_KEY]

        invalidations_date_settings = self.cfg_params.get(CONFIG_INVALIDATIONS_DATE, {})
        self.invalidations_since = invalidations_date_settings.get(CONFIG_INVALIDATIONS_SINCE, 'last').strip()
        self.invalidations_until = invalidations_date_settings.get(CONFIG_INVALIDATIONS_UNTIL, 'now').strip()

        output = self.cfg_params.get(CONFIG_OUTPUT_KEY, {})
        self.output_bucket = output.get(CONFIG_OUTPUT_BUCKET, '').strip()
        self.output_table = output.get(CONFIG_OUTPUT_TABLE, '').strip()
        self.incremental = output.get(CONFIG_OUTPUT_INCREMENTAL, False)

        self.check_input_tables()

    def check_input_tables(self):

        if self.mode == 'invalidations':
            return

        tables = glob(os.path.join(self.tables_in_path, '*.csv'))

        if len(tables) == 0:
            logging.error("No input tables provided.")

        if self.mode not in ALLOWED_MODES:
            logging.error(f"Invalid mode provided: {self.mode}. Must be one of allowed: {', '.join(ALLOWED_MODES)}.")
            sys.exit(1)

        else:
            required_table = f"{self.mode}.csv"
            required_table_full_path = os.path.join(self.tables_in_path, required_table)
            if required_table_full_path not in tables:
                logging.error(f"Mode {self.mode} requires table {required_table} on the input mapping.")
                sys.exit(1)
            else:
                self.input_table = required_table_full_path

    def _retrieve_and_write_transactions(self, batch: list, writer: ResultWriter):

        if len(batch) == 0:
            return

        results = self.client.get_shops_by_transactions(transaction_batch=batch)
        writer.writerows(results)

    def process_transactions(self, reader: csv.DictReader):

        request_counter = 0

        writer = ResultWriter(self.tables_out_path, 'transactions', self.incremental, self.destination)

        if 'customId' not in reader.fieldnames:
            logging.error("Field 'customId' is required for transactions. Please, provide this non-empty field.")
            sys.exit(1)

        rows_to_send = []
        try:
            for row in reader:
                _tmp = dict()
                for key, value in row.items():
                    if key in TRANSACTIONS_ATTRIBUTES_CAST and value.strip() != '':
                        try:
                            _tmp[key] = TRANSACTIONS_ATTRIBUTES_CAST[key](value)
                        except ValueError:
                            logging.exception(''.join([f"Could not convert value {value} in column {key}",
                                                       f" to type {str(TRANSACTIONS_ATTRIBUTES_CAST[key])}."]),
                                              exc_info=True)
                            raise
                    else:
                        _tmp[key] = str(value)
                rows_to_send += [_tmp]

                if len(rows_to_send) == self.TRANSACTIONS_BATCH_SIZE:
                    # Process if limit is reached. Then reset back to empty list.
                    self._retrieve_and_write_transactions(rows_to_send, writer)
                    request_counter += 1
                    rows_to_send = []

                    if request_counter % 50 == 0:
                        logging.info(f"Made {request_counter} batch requests so far.")

                else:
                    continue
        except _csv.Error as err:
            raise UserException(f'Input file processing failed {err}', f'Last row: {row}') from err

        # Process final batch
        self._retrieve_and_write_transactions(rows_to_send, writer)
        logging.info(f"Made {request_counter + 1} batch requests in total.")

    def determine_date(self):
        if self.invalidations_since == 'last' or self.invalidations_since.replace(' ', '') == 'last-1':
            state = self.get_state_file()
            if state is None:
                logging.warn(f"No state file found. Defaulting to: {self.INVALIDATIONS_DEFAULT_RANGE_SINCE}.")
                _inv_date_since = dateparser.parse(self.INVALIDATIONS_DEFAULT_RANGE_SINCE)
            else:
                _ts = state.get('invalidations')

                if _ts is None:
                    logging.warn("No last state found for invalidations. Defaulting to past 7 days.")
                    _inv_date_since = dateparser.parse(self.INVALIDATIONS_DEFAULT_RANGE_SINCE)

                else:
                    _inv_date_since = datetime.utcfromtimestamp(_ts)

                    if self.invalidations_since.replace(' ', '') == 'last-1':
                        _inv_date_since = _inv_date_since - timedelta(days=1)

            _inv_date_until = dateparser.parse(self.INVALIDATIONS_DEFAULT_RANGE_UNTIL)

        elif self.invalidations_since.replace(' ', '') == 'last-1':
            state = self.get_state_file()
            if state is None:
                logging.warn(f"No state file found. Defaulting to: {self.INVALIDATIONS_DEFAULT_RANGE_SINCE}.")
                _inv_date_since = dateparser.parse(self.INVALIDATIONS_DEFAULT_RANGE_SINCE)
            else:
                _ts = state.get('invalidations')

        else:
            _inv_date_since = dateparser.parse(self.invalidations_since)
            _inv_date_until = dateparser.parse(self.invalidations_until)
            if _inv_date_since is None:
                logging.error(f"Invalid date since {self.invalidations_since} provided.")
                sys.exit(1)
            elif _inv_date_until is None:
                logging.error(f"Invalid date until {self.invalidations_until} provided.")
                sys.exit(1)
            else:
                pass

        return _inv_date_since, _inv_date_until

    def determine_destination(self):
        if self.output_bucket is None or self.output_bucket == '':
            _output_bucket = f"in.c-{self.cmp_id.replace('.', '-')}-{self.cfg_id}"
        else:
            _output_bucket = self.output_bucket

        if self.output_table is None or self.output_table == '':
            _output_table = self.mode
        else:
            _output_table = self.output_table

        return f'{_output_bucket}.{_output_table}'

    def get_invalidations(self):

        current_ts = int(time.time())

        inv_date_since, inv_date_until = self.determine_date()
        dt_since = inv_date_since.strftime(DT_RFC_3399)
        dt_until = inv_date_until.strftime(DT_RFC_3399)

        logging.info(f"Downloading invalidations from date {dt_since} to date {dt_until}.")

        writer = ResultWriter(self.tables_out_path, 'invalidations', self.incremental, self.destination)

        invalidations_range = self.client.get_invalidations_range(date_from=inv_date_since, date_to=inv_date_until)
        if invalidations_range['itemCount'] == 0:
            logging.info("No new invalidations")
            self.write_state_file({'invalidations': int(inv_date_until.timestamp())})
            return

        from_id = invalidations_range['fromId']
        to_id = invalidations_range['toId']
        is_complete = False

        while is_complete is False:
            invalidations = self.client.get_invalidations(from_id=from_id, to_id=to_id)

            batches = invalidations['data']['batches']

            for batch in batches:
                all_data = [{'type': batch['type'], 'level': batch['level'], 'timestamp': current_ts,
                             'run_id': self.run_id, 'since': dt_since, 'until': dt_until, 'uid': x}
                            for x in batch['ids']]
                writer.writerows(all_data)

            is_complete = invalidations['paging']['lastPage']
            from_id = invalidations['paging'].get('lastItemId')

        self.write_state_file({'invalidations': int(inv_date_until.timestamp())})

    def get_shops(self, reader: csv.DictReader):

        request_counter = 0

        if 'id' not in reader.fieldnames:
            logging.error("Field 'id' must be provided in the input table 'shops.csv'.")
            sys.exit(1)

        writer = ResultWriter(self.tables_out_path, 'shops', self.incremental, self.destination)

        for row in reader:
            shop_data = self.client.get_shop_by_id(row['id'])
            request_counter += 1
            writer.writerows([shop_data])

            if request_counter % 100 == 0:
                logging.info(f"Made {request_counter} requests so far.")

        logging.info(f"Made {request_counter} requests in total.")

    def get_merchants(self, reader: csv.DictReader):

        request_counter = 0

        if 'id' not in reader.fieldnames:
            logging.error("Field 'id' must be provided in the input table 'merchants.csv'.")
            sys.exit(1)

        writer = ResultWriter(self.tables_out_path, 'merchants', self.incremental, self.destination)

        for row in reader:
            merchant_data = self.client.get_merchant_by_id(row['id'])
            request_counter += 1
            writer.writerows([merchant_data])

            if request_counter % 100 == 0:
                logging.info(f"Made {request_counter} requests so far.")

        logging.info(f"Made {request_counter} requests in total.")

    def run(self):

        self.destination = self.determine_destination()

        if self.mode == 'invalidations':
            self.get_invalidations()

        else:
            with open(self.input_table) as mode_table:

                _rdr = csv.DictReader(mode_table)

                if self.mode == 'transactions':
                    self.process_transactions(_rdr)
                elif self.mode == 'merchants':
                    self.get_merchants(_rdr)
                elif self.mode == 'shops':
                    self.get_shops(_rdr)
                else:
                    logging.error("Unsupported mode.")
                    sys.exit(1)

        logging.info("Component finished.")
