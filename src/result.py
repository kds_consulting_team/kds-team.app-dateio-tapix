import os
import csv
import json
import logging

FIELDS_TRANSACTIONS = ['customId', 'result', 'handle', 'shop_uid']
FIELDS_R_TRANSACTIONS = FIELDS_TRANSACTIONS
PK_TRANSACTIONS = ['customId']
JSON_TRANSACTIONS = []

FIELDS_INVALIDATIONS = ['uid', 'type', 'level', 'run_id', 'timestamp', 'since', 'until']
FIELDS_R_INVALIDATIONS = FIELDS_INVALIDATIONS
PK_INVALIDATIONS = ['uid', 'type', 'level', 'run_id', 'timestamp']
JSON_INVALIDATIONS = []

FIELDS_MERCHANTS = ['uid', 'name', 'category', 'logo']
FIELDS_R_MERCHANTS = FIELDS_MERCHANTS
PK_MERCHANTS = ['uid']
JSON_MERCHANTS = []

FIELDS_SHOPS = ['uid', 'type', 'tags', 'merchantUid', 'location_address_street', 'location_address_city',
                'location_address_zip', 'location_address_country', 'location_address_unid',
                'location_coordinates_lat', 'location_coordinates_long', 'url', 'googlePlaceId']
FIELDS_R_SHOPS = FIELDS_SHOPS
PK_SHOPS = ['uid']
JSON_SHOPS = ['tags']


class ResultWriter:

    def __init__(self, tableOutPath, tableName, incremental, destination):

        self.path = tableOutPath
        self.table_name = tableName
        self.table = tableName + '.csv'
        self.table_path = os.path.join(self.path, self.table)
        self.fields = eval(f'FIELDS_{tableName.upper().replace("-", "_")}')
        self.fields_json = eval(f'JSON_{tableName.upper().replace("-", "_")}')
        self.primary_key = eval(f'PK_{tableName.upper().replace("-", "_")}')
        self.fields_renamed = eval(f'FIELDS_R_{tableName.upper().replace("-", "_")}')
        self.incremental = incremental
        self.destination = destination

        self.create_manifest()
        self.create_writer()

    def create_manifest(self):

        template = {
            'incremental': self.incremental,
            'primary_key': self.primary_key,
            'columns': self.fields_renamed,
            'destination': self.destination
        }

        path = self.table_path + '.manifest'

        with open(path, 'w') as manifest:
            json.dump(template, manifest)

    def create_writer(self):

        self.writer = csv.DictWriter(open(self.table_path, 'w', newline=''), fieldnames=self.fields,
                                     restval='', extrasaction='ignore', quotechar='\"', quoting=csv.QUOTE_ALL,
                                     lineterminator='\n')

    def writerows(self, listToWrite, parentDict=None):

        for row in listToWrite:
            row_f = self.flatten_json(x=row)

            if self.fields_json != []:
                for field in self.fields_json:
                    try:
                        row_f[field] = json.dumps(row.get(field, []))
                    except json.JSONDecodeError:
                        logging.warning(f"Could not decode JSON value {row.get(field, [])}.")
                        row_f[field] = json.dumps([])

            _dictToWrite = {}

            for key, value in row_f.items():

                if key in self.fields:
                    _dictToWrite[key] = value
                else:
                    continue

            if parentDict is not None:
                _dictToWrite = {**_dictToWrite, **parentDict}

            self.writer.writerow(_dictToWrite)

    def flatten_json(self, x, out=None, name=''):
        if out is None:
            out = dict()

        if type(x) is dict:
            for a in x:
                self.flatten_json(x[a], out, name + a + '_')
        else:
            out[name[:-1]] = x

        return out
