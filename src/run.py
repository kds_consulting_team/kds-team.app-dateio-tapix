import logging
import sys

from component import Component, UserException

if __name__ == '__main__':
    c = Component()
    try:
        c.run()
    except UserException as exc:
        detail = ''
        if len(exc.args) > 1:
            detail = exc.args[1]
        logging.exception(exc, extra={"full_message": detail})
        exit(1)
    except Exception as e:
        logging.exception(e)
        sys.exit(1)
