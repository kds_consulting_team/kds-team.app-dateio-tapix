import json
import logging
import sys

from kbc.client_base import HttpClientBase
from urllib.parse import urljoin
from datetime import datetime

DT_RFC_3399 = '%Y-%m-%dT%H:%M:%SZ'


class Client(HttpClientBase):
    TAPIX_URL = 'https://tapix.dateio.eu/v5/'
    INVALIDATIONS_PAGE_SIZE = 250000
    INVALIDATIONS_LEVELS = 'shallow,deep,solved'

    def __init__(self, username, password):
        super().__init__(self.TAPIX_URL, auth=(username, password))

    def get_shops_by_transactions(self, transaction_batch: list) -> list:
        '''Returns a list of shops matched for each transaction in a batch'''

        shops_url = urljoin(self.base_url, 'shops/findByCardTransactionBatch')
        shops_hdr = {'Content-Type': 'application/json'}
        shops_bdy = {'requests': transaction_batch}

        shops_rsp = self.post_raw(url=shops_url, headers=shops_hdr, data=json.dumps(shops_bdy), timeout=50)
        shops_rsp_sc = shops_rsp.status_code

        if shops_rsp_sc == 200:
            return shops_rsp.json()

        elif shops_rsp_sc == 401:
            logging.error("Invalid credentials.")
            sys.exit(1)

        else:
            logging.error(' '.join(["Could not get data about shops from transactions.",
                                    f"Received: {shops_rsp_sc} - {shops_rsp.json()}."]))

            # logging.debug(f"URL: {shops_url}.\nHeaders: {shops_hdr}.")
            # logging.debug(f"Body: {shops_bdy}.")
            sys.exit(1)

    def get_shop_by_id(self, shop_id: str) -> dict:
        '''Returns detailed information about a shop from its handled or uid'''

        shop_url = urljoin(self.base_url, f'shops/{shop_id}')

        shop_rsp = self.get_raw(url=shop_url)
        shop_rsp_sc = shop_rsp.status_code

        if shop_rsp_sc == 200:
            return shop_rsp.json()

        elif shop_rsp_sc == 401:
            logging.error("Invalid credentials.")
            sys.exit(1)

        else:
            logging.warning(' '.join([f"Could not get detailed information for a shop with id {shop_id}.",
                                      f"Received: {shop_rsp_sc}."]))
            return {'uid': shop_id, 'type': 'NOT FOUND', 'merchantUid': 'NOT FOUND'}

    def get_merchant_by_id(self, merchant_id: str) -> dict:
        '''Returns detailed information about a merchant from its handle or uid'''

        merchant_url = urljoin(self.base_url, f'merchants/{merchant_id}')

        merchant_rsp = self.get_raw(url=merchant_url)
        merchant_rsp_sc = merchant_rsp.status_code

        if merchant_rsp_sc == 200:
            return merchant_rsp.json()

        elif merchant_rsp_sc == 401:
            logging.error("Invalid credentials.")
            sys.exit(1)

        else:
            logging.warning(' '.join([f"Could not get detailed information for a merchant with id {merchant_id}.",
                                      f"Received: {merchant_rsp_sc}."]))
            return {'uid': merchant_id, 'name': 'NOT FOUND', 'category': 'NOT FOUND'}

    def get_invalidations_range(self, date_from: datetime, date_to: datetime = None) -> dict:
        '''Returns a numeric range of all invalidations within specified time period'''

        date_from_rfc = date_from.strftime(DT_RFC_3399)
        date_to_rfc = date_to.strftime(DT_RFC_3399) if date_to is not None else None

        invalidations_url = urljoin(self.base_url, 'invalidations/item/range')
        invalidations_par = {'from': date_from_rfc, 'to': date_to_rfc}

        invalidations_rsp = self.get_raw(url=invalidations_url, params=invalidations_par)
        invalidations_rsp_sc = invalidations_rsp.status_code

        if invalidations_rsp_sc == 200:
            return invalidations_rsp.json()

        elif invalidations_rsp_sc == 401:
            logging.error("Invalid credentials.")
            sys.exit(1)

        else:
            logging.error(' '.join(["Could not download invalidations item range.",
                                    f"Received: {invalidations_rsp_sc}."]))
            sys.exit(1)

    def get_invalidations(self, from_id: str, to_id: str = None) -> dict:
        '''Returns a list of all invalidated items in the API'''

        invalidations_url = urljoin(self.base_url, 'invalidations')
        invalidations_par = {
            'fromId': from_id,
            'toId': to_id,
            'level': self.INVALIDATIONS_LEVELS,
            'pageSize': self.INVALIDATIONS_PAGE_SIZE
        }

        invalidations_rsp = self.get_raw(url=invalidations_url, params=invalidations_par)
        invalidations_rsp_sc = invalidations_rsp.status_code

        if invalidations_rsp_sc == 200:
            return invalidations_rsp.json()

        elif invalidations_rsp_sc == 401:
            logging.error("Invalid credentials.")
            sys.exit(1)

        else:
            logging.error(' '.join(["Could not download invalidations item range.",
                                    f"Received: {invalidations_rsp_sc}."]))
            sys.exit(1)
